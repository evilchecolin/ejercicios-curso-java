<%-- 
    Document   : showCars
    Created on : 26/03/2020, 06:48:54 PM
    Author     : checo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        Lista de carros <br/>
        <c:forEach items = "${cars}"  var = "car">
            Carro [<c:out value = "${car.model}"/>,<c:out value = "${car.color}"/>,<c:out value = "${car.plate}"/>]<br/>
        </c:forEach>
    </body>
</html>
