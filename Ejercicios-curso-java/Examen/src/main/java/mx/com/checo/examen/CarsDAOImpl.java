/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.checo.examen;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author checo
 */
public class CarsDAOImpl implements CarsDAO {

    private static List<Car> cars = new ArrayList<>();
    
    public CarsDAOImpl() {
    }

    @Override
    public List<Car> getAllCars() {
        return cars;
    }

    @Override
    public boolean addNewCar(Car car) {
        cars.add(car);
        return true;
    }
    
}
