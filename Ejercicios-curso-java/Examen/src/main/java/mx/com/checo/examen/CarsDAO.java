/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.checo.examen;

import java.util.List;

/**
 *
 * @author checo
 */
public interface CarsDAO {

    public static CarsDAO getInstance() {
        return new CarsDAOImpl();
    }

    public List<Car> getAllCars();
    
    public boolean addNewCar(Car car);

}
