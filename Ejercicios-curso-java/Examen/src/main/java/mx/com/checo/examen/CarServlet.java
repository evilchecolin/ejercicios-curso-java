/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.checo.examen;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author checo
 */
@WebServlet("/car")
public class CarServlet extends HttpServlet {

    /**
     * Este es para ver la lista de Carros *
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {

        try {
            request.setAttribute("cars", CarsDAO.getInstance().getAllCars());
            request.getRequestDispatcher("/showCars.jsp").forward(request, response);
        } catch (ServletException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }

    /**
     * Este es para agregar un Carro *
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {

        CarsDAO.getInstance().addNewCar(new Car(request.getParameter("model"), request.getParameter("model"), request.getParameter("model")));

    }

}
