/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;

/**
 *
 * @author checo
 */
public class NewMain {

    private static final BigDecimal min = BigDecimal.valueOf(290.00);
    private static final BigDecimal c = BigDecimal.valueOf(518400);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        String hora1 = "164715";
        int tasaP = 0;
        BigDecimal monto = new BigDecimal("10.0");
        int fecha1 = 20200326;
        BigDecimal minimo = new BigDecimal(290.00);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        StringBuffer fechaOri = new StringBuffer();
        fechaOri.append(fecha1);
        if (fechaOri.length() != 8) {
            throw new Exception("La fecha original no cumple el formato yyyyMMdd");
        }
        if (hora1.length() < 6 && hora1.length() > 0) {
            for (int cont = 0; cont < (6 - hora1.length()); cont++) {
                fechaOri.append("0");
            }
        }
        fechaOri.append(hora1);
        if (fechaOri.length() != 14) {
            throw new Exception("La hora original no cumple el formato HHmmss");
        }
        long originalTime = formatter.parse(fechaOri.toString()).getTime();
        long actualTime = System.currentTimeMillis();
        long takenTime = actualTime - originalTime;
        if (takenTime < 0) {
            throw new Exception(
                    "La fecha y hora originales son posteriores a la fecha y hora maquina actuales");
        }
        if (takenTime <= 300000) {
            System.out.println("0");
        }

        if (takenTime % 60000 != 0) { // Redondeamos al minuto siguiente (FLOOR) //Se quito el redondeo arriba
            takenTime = takenTime - (takenTime % 60000);
        }
        long minutosRetraso = takenTime / 60000; // minutos de retraso =
        // (#milisegundos / 1000) /
        // 60
        BigDecimal salida = monto.multiply(BigDecimal.valueOf(2 * minutosRetraso * (tasaP / 100)));
        salida = salida.divide(c, 2, RoundingMode.HALF_UP);
        System.out.println(salida.compareTo(min) < 0 ? min : salida);

    }
}
