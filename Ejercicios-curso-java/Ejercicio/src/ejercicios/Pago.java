/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

import java.io.Serializable;

/**
 *
 * @author checo
 */
public class Pago implements Serializable {
    
    private Long tp_clave;
    private String tp_nombre;
    /**
    public Pago(Long tp_clave, String tp_nombre) {
        this.tp_clave = tp_clave;
        this.tp_nombre = tp_nombre;
    }
    * Constructor por defecto en este caso lo reemplace con el patron BUILDER
    **/

    /**
     * @return the tp_clave
     */
    public Long getTp_clave() {
        return tp_clave;
    }

    /**
     * @param tp_clave the tp_clave to set
     */
    public void setTp_clave(Long tp_clave) {
        this.tp_clave = tp_clave;
    }

    /**
     * @return the tp_nombre
     */
    public String getTp_nombre() {
        return tp_nombre;
    }

    /**
     * @param tp_nombre the tp_nombre to set
     */
    public void setTp_nombre(String tp_nombre) {
        this.tp_nombre = tp_nombre;
    }
    
    public Pago withTp_nombre(String tp_nombre){
        this.tp_nombre = tp_nombre;
        return this;
    }
    
    public Pago withTp_clave(Long tp_clave){
        this.tp_clave = tp_clave;
        return this;
    }
    
    @Override
    public String toString(){
        return "Pago [Clave:"+this.tp_clave+", Nombre:"+this.tp_nombre+"]";
    }
    
}
