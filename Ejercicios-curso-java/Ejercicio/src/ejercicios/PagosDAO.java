/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author checo
 */
public interface PagosDAO {

    String DRIVER = "oracle.jdbc.OracleDriver";
    String URL = "jdbc:oracle:thin:enlace/enlace@192.168.17.83:1521:RAP";
    String QUERY_SELECT = "select * from bmx_t_pago";
    String QUERY_INSERT = "insert into bmx_t_pago (tp_clave, tp_descripcion) values (?,?)";
    String QUERY_DELETE = "delete from bmx_t_pago where tp_clave = ?";
    String QUERY_UPDATE = "update bmx_t_pago set tp_descripcion = ? where tp_clave = ?";
    String PAGO_CLAVE = "tp_clave";
    String PAGO_NOMBRE = "tp_descripcion";

    public List<Pago> getPagos() throws SQLException;

    public void insetPagos(final List<Pago> pagos) throws SQLException;

    public void deletePagos(final List<Pago> pagos) throws SQLException;

    public void updatePagos(final List<Pago> pagos) throws SQLException;

    /*Fabrica abstracta donde se puede establecer una instancia diferente de a cuerdo a un parametro recibido*/
    static PagosDAO newInstance(boolean isTransactional) {
        return isTransactional ? new PagosTransactionalDAOImpl() : new PagosDAOImpl();
    }

}
