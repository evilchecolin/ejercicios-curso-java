/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author checo
 */
public class PagosDAOImpl implements PagosDAO {

    public PagosDAOImpl() {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Pago> getPagos() throws SQLException {
        List<Pago> pagos = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL);
                PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SELECT);
                ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                pagos.add(new Pago().withTp_clave(resultSet.getLong(PAGO_CLAVE)).withTp_nombre(resultSet.getString(PAGO_NOMBRE)));
            }
            return pagos;
        } catch (SQLException e) {
            throw e;
        }
    }

    @Override
    public void insetPagos(List<Pago> pagos) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletePagos(List<Pago> pagos) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updatePagos(List<Pago> pagos) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
