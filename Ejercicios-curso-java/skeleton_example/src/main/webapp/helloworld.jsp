<%-- 
    Document   : helloworld
    Created on : 19/03/2020, 06:51:56 PM
    Author     : checo
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="mx.com.miraiit.Pago"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>mY HELLO WORLD PAGE</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h1><% out.println("Hello world to my friends with scriptlets");%></h1>
        <h1>${"Heelo world with expression language (EL)"}</h1>
        <h1><%="Hello world with expressions "%></h1>
        <h1><c:out value="Hello world from jstl"/></h1>


        <form name="createCube" action="all" method="post">
            Id pago: <input type="text" name="id" value="0"/>
            <br/>
            Descripcion: <input type="text" name="descripcion" value="0"/>
            <br/>
            <input type="submit" value="Create pago" />
        </form>


        <jsp:useBean id="pagoBean" class="mx.com.miraiit.Pago" scope="session"/>
    </body>
</html>
