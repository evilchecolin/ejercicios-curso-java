/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miraiit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;

/**
 *
 * @author checo
 */
public class PagosDAOImpl implements PagosDAO {

    @Override
    public List<Pago> getPagos() throws SQLException {
        List<Pago> pagos = new ArrayList<>();
        try (Connection connection = new ConnectionManager().withDatasource().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(QUERY_SELECT);
                ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                pagos.add(new Pago().withTp_clave(resultSet.getLong(PAGO_CLAVE)).withTp_nombre(resultSet.getString(PAGO_NOMBRE)));
            }
            return pagos;
        } catch (SQLException e) {
            throw e;
        } catch (NamingException ex) {
            throw new SQLException("NamingException: ", ex);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("ClassNotFoundException: ", ex);
        }
    }

    @Override
    public void insetPagos(List<Pago> pagos) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deletePagos(List<Pago> pagos) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updatePagos(List<Pago> pagos) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
