/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miraiit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;

/**
 *
 * @author checo
 */
public class PagosTransactionalDAOImpl implements PagosDAO {

    @Override
    public void insetPagos(List<Pago> pagos) throws SQLException {
        try (Connection connection = new ConnectionManager().withDatasource().getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_INSERT)) {
                for (Pago pago : pagos) {
                    preparedStatement.setLong(1, pago.getTp_clave());
                    preparedStatement.setString(2, pago.getTp_nombre());

                    if (preparedStatement.executeUpdate() < 1) {
                        System.out.println("No se inserto el registro.");
                    }
                }

                connection.commit();
            } catch (SQLException ex) {
                connection.rollback();
                throw ex;
            }

        } catch (SQLException e) {
            throw e;
        } catch (NamingException ex) {
            throw new SQLException("NamingException: ", ex);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("ClassNotFoundException: ", ex);
        }
    }

    @Override
    public void deletePagos(List<Pago> pagos) throws SQLException {
        try (Connection connection = new ConnectionManager().withDatasource().getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_DELETE)) {
                for (Pago pago : pagos) {
                    preparedStatement.setLong(1, pago.getTp_clave());

                    if (preparedStatement.executeUpdate() < 1) {
                        System.out.println("No se elmino el registro.");
                    }
                }

                connection.commit();
            } catch (SQLException ex) {
                connection.rollback();
                throw ex;
            }

        } catch (SQLException e) {
            throw e;
        } catch (NamingException ex) {
            throw new SQLException("NamingException: ", ex);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("ClassNotFoundException: ", ex);
        }
    }

    @Override
    public void updatePagos(List<Pago> pagos) throws SQLException {
        try (Connection connection = new ConnectionManager().withDatasource().getConnection()) {
            connection.setAutoCommit(false);
            try (PreparedStatement preparedStatement = connection.prepareStatement(QUERY_UPDATE)) {
                for (Pago pago : pagos) {
                    preparedStatement.setString(1, pago.getTp_nombre());
                    preparedStatement.setLong(2, pago.getTp_clave());

                    if (preparedStatement.executeUpdate() < 1) {
                        System.out.println("No se actualizo el registro.");
                    }
                }

                connection.commit();
            } catch (SQLException ex) {
                connection.rollback();
                throw ex;
            }

        } catch (SQLException e) {
            throw e;
        } catch (NamingException ex) {
            throw new SQLException("NamingException: ", ex);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("ClassNotFoundException: ", ex);
        }
    }

    @Override
    public List<Pago> getPagos() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
