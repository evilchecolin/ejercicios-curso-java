/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miraiit;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author checo
 */
public class PagosServlet extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try (PrintWriter out = res.getWriter()) {
            PagosDAO dao = PagosDAO.newInstance(false);
            dao.getPagos().forEach(pago -> System.out.println(pago));
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            
            dao.getPagos().forEach(pago -> out.println(pago + "<br/>"));
            
            out.println("<br/><br/><form action='all' method='post'>");
            
            out.println("Identificador: <input type='text' name='id'><br/><br/>");
            out.println("Nombre: <input type='text' name='descripcion'><br/><br/>");
            out.println("<input type='submit'>");
            
            out.println("</form>");
            
            
            out.println("</body>");
            out.println("</html>");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try (PrintWriter out = res.getWriter()) {
            PagosDAO dao = PagosDAO.newInstance(true);
            
            List<Pago> pagos = new ArrayList<Pago>();
            pagos.add(new Pago().withTp_clave(Long.parseLong(req.getParameter("id"))).withTp_nombre(req.getParameter("descripcion")));
            dao.insetPagos(pagos);
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> Se inserto correctamente </h1>");
            
            out.println("</body>");
            out.println("</html>");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
}
