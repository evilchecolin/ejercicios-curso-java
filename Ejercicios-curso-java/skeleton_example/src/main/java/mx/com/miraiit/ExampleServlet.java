/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miraiit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author checo
 */
@WebServlet("/example")
public class ExampleServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType(MediaType.TEXT_PLAIN);

        try (PrintWriter pw = response.getWriter()) {
            pw.println("LM [" + this + "]");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            BufferedReader bf = request.getReader();
            String line;
            while ((line = bf.readLine()) != null) {
                System.out.println(line);
            }
            response.setStatus(200);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
