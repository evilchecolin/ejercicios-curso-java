/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miraiit;

import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author checo
 */
@WebServlet("/login")
public class Login extends HttpServlet {

    public void doPost(HttpServletRequest rq, HttpServletResponse rs) {
        try (PrintWriter out = rs.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewServlet</title>");
            out.println("</head>");
            out.println("<body>");

            out.println("Usuario: " + rq.getParameter("usuario"));

            out.println("<br/>");
            out.println("Password: " + rq.getParameter("password"));

            out.println("<br/>");

            for (String ms : rq.getParameterValues("check")) {
                out.println("<br/>"+ms+"<br/>");
            }

            out.println("</body>");
            out.println("</html>");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
