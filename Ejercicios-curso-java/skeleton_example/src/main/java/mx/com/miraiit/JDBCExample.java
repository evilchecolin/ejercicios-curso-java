/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miraiit;

import java.sql.SQLException;

/**
 *
 * @author checo
 */
public class JDBCExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            PagosDAO dao = PagosDAO.newInstance(false);
            dao.getPagos().forEach(pago -> System.out.println(pago));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
