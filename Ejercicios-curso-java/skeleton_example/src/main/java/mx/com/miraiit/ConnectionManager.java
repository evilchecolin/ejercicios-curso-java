/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.miraiit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author checo
 */
public class ConnectionManager {

    private static final String DRIVER = "oracle.jdbc.OracleDriver";
    private static final String URL = "jdbc:oracle:thin:enlace/enlace@192.168.17.83:1521:RAP";

    private DataSource dataSource;
    private boolean WITH_DATASOURCE;

    /* Esto es patron builder */
    public ConnectionManager withDatasource() {
        WITH_DATASOURCE = true;
        return this;
    }
    
    /* Esto seria patron Factory */
    public Connection getConnection() throws NamingException, SQLException, ClassNotFoundException {
        if (WITH_DATASOURCE) {
            Context ctx = new InitialContext();
            dataSource = (DataSource) ctx.lookup("java:/ejemplo");
            return dataSource.getConnection();
        } else {
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL);
        }
    }

}
