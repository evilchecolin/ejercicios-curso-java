/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Optional;

/**
 *
 * @author checo
 */
public class PrincipalGame {

    interface Playable {

        void walk(double x, double y);

        default void setGameName(String name) {
            System.out.println(Optional.ofNullable(name).orElseThrow(() -> {throw new RuntimeException("hola k ase");}));
        }
    }

    interface Gameable {

        void startGame();

        default void setGameName(String name) {
            System.out.println(name);
        }
    }

    interface Soundable {

        void playMusic(String song);

        default void setGameName(String name) {
        	System.out.println(Optional.ofNullable(name).orElse("Otro mensaje"));
        }
    }

    private static String gameName = "ChecoGame";
    private static double playerPosX = 0;
    private static double playerPosY = 0;

    public static void main(String[] args) {
        Playable p = (x, y) -> {
            playerPosX = playerPosX + x;
            playerPosY = playerPosY + y;
        };
        p.walk(1, 1);
        p.walk(1, 1);
        p.walk(1, 1);
        System.out.println("Pos [" + playerPosX + "],[" + playerPosY + "]");
        Gameable g = () -> {
            p.setGameName(gameName);
        };
        g.startGame();

        Soundable s = (song) -> {
        };
        s.setGameName(gameName);
    }

}

