import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Alumno implements Serializable {

	private final long serialVersionUID = -7171089273091270L;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombreCurso;
	private long numeroCurso;

	public Alumno(String nombre, String apellidoPaterno, String apellidoMaterno, String nombreCurso) {
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.nombreCurso = nombreCurso;
	}

	public long getNumeroCurso() {
		return numeroCurso;
	}

	public void setNumeroCurso(long numeroCurso) {
		this.numeroCurso = numeroCurso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNombreCurso() {
		return nombreCurso;
	}

	public void setNombreCurso(String nombreCurso) {
		this.nombreCurso = nombreCurso;
	}

	@Override
	public String toString() {
		return this.nombre + ", " + this.apellidoPaterno + ", " + this.apellidoMaterno + "," + this.nombreCurso + ", "
				+ this.numeroCurso;
	}

	public static void main(String[] args) {
		List<Alumno> alumnos = new ArrayList<>();
		alumnos.add(new Alumno("Sergio David", "Gomez", "Rodriguez","Curso Java"));
		alumnos.add(new Alumno("Martin", "Gonzales", "Montoya","Curso Java"));
		alumnos.add(new Alumno("Angel", "Chavez", "Chavez","Curso Java"));
		alumnos.add(new Alumno("Edgar", "Garcia", "Davalos","Curso Java"));
		alumnos.add(new Alumno("Thelma", "Perez", "Casas","Curso Java"));
		alumnos.add(new Alumno("Miriam Azucena", "Valdez", "Cedillo","Curso Java"));
		alumnos.add(new Alumno("Kevin", "Aburto", "Muela", "Curso Java"));
		alumnos.add(new Alumno("Sandra", "Wuesk", "Wuesk","Curso Java"));
		alumnos.add(new Alumno("Jorge", "Rodriguez", "Flores","Curso Java"));

		alumnos.forEach(alumno -> System.out.println("Alumno[" + alumno + "]"));
		System.out.println("Numero de alumnos [" + alumnos.size() + "]");

		for (Alumno alumno : alumnos) {
			if (alumno.getNombre().charAt(alumno.getNombre().length() - 1) == 'a') {
				System.out.println("Alumno que termina con a [" + alumno + "]");
			}
		}

		alumnos.stream().limit(5).forEach(alumno -> System.out.println("Alumno de los primeros 5 [" + alumno + "]"));

	}

}

