package mx.com.praxis.services.impl;

import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;

import mx.com.praxis.daos.PersonDao;
import mx.com.praxis.dtos.PersonDto;
import mx.com.praxis.entities.Person;
import mx.com.praxis.services.PersonService;

@Stateless
public class PersonServiceImpl implements PersonService {

	private final PersonDao personDao = PersonDao.getInstance();

	@Override
	public List<PersonDto> getPeople(final int page, final int size) {
		return personDao.findAll(page, size).stream().map(p -> toPersonDto(p)).collect(Collectors.toList());
	}

	@Override
	public PersonDto findById(final long id) {
		return toPersonDto(personDao.findById(id));
	}

	@Override
	public void update(final PersonDto person) {
		final Person entity = new Person();

		entity.setId(person.getId());
		entity.setFirstName(person.getFirstName());
		entity.setLastName(person.getLastName());

		personDao.update(entity);
	}

	@Override
	public void insert(final PersonDto person) {
		final Person entity = new Person();

		entity.setId(person.getId());
		entity.setFirstName(person.getFirstName());
		entity.setLastName(person.getLastName());
		entity.setAge(-1);
		entity.setEmail("no@email.com");

		personDao.insert(entity);
	}

	private PersonDto toPersonDto(final Person person) {
		return PersonDto.builder().firstName(person.getFirstName()).id(person.getId()).lastName(person.getLastName())
				.build();
	}

}
