/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.daos.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import mx.com.praxis.daos.PersonDao;
import mx.com.praxis.entities.Person;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
public class PersonDaoImpl implements PersonDao {

	private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("DBUnit");

	@SuppressWarnings("unchecked")
	@Override
	public List<Person> findAll(final int page, final int size) {
		final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();

		final Query query = entityManager.createNamedQuery("Person.findAll");
		query.setFirstResult((page - 1) * size);
		query.setMaxResults(size);

		final List<Person> results = query.getResultList();
		entityManager.close();

		return results;
	}

	@Override
	public Person findById(final Long id) {
		final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
		final Query query = entityManager.createNamedQuery("Person.findById");
		query.setParameter("id", id);

		final Person result = (Person) query.getSingleResult();
		entityManager.close();

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Person> findByFistName(final int page, final int size, final String firstName) {
		final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();

		final Query query = entityManager.createNamedQuery("Person.findByFistName");
		query.setFirstResult((page - 1) * size);
		query.setMaxResults(size);
		query.setParameter("firstName", firstName);

		final List<Person> results = query.getResultList();
		entityManager.close();

		return results;
	}

	@Override
	public void update(final Person entity) {
		final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();

		final Person emPerson = entityManager.find(Person.class, entity.getId());

		entityManager.getTransaction().begin();
		emPerson.setLastName(entity.getLastName());
		emPerson.setFirstName(entity.getFirstName());
		entityManager.merge(emPerson);
		entityManager.getTransaction().commit();

		entityManager.close();
	}

	@Override
	public void insert(final Person entity) {
		final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();

		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();

		entityManager.close();
	}
}
