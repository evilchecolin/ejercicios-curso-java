/*
 * Copyright (c) 3/18/20
 * Mexico D.F.
 * All rights reserved.
 *
 * THIS SOFTWARE IS  CONFIDENTIAL INFORMATION PROPRIETARY OF
 * THIS INFORMATION SHOULD NOT BE DISCLOSED AND MAY ONLY BE USED IN ACCORDANCE THE TERMS DETERMINED BY THE COMPANY ITSELF.
 */
package mx.com.praxis.daos;

import mx.com.praxis.daos.impl.StudentDaoImpl;
import mx.com.praxis.entities.Student;

/**
 * <p>
 * </p>
 *
 * @author Marco Acevedo
 * @version Programming
 * @since Programming
 */
public interface StudentDao extends GenericDao<Student, Long> {
	Student findByIdPerson(long idPerson);

	static StudentDao getInstance() {
		return new StudentDaoImpl();
	}
}
