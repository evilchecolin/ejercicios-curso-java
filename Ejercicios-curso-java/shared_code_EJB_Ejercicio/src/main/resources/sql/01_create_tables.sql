CREATE SEQUENCE people_seq;
CREATE SEQUENCE student_seq;

CREATE TABLE person (
    id integer DEFAULT nextval('people_seq'),
    first_name varchar(50),
    last_name varchar(50),
    age int,
    email varchar(100),

    CONSTRAINT id_person PRIMARY KEY (id)
);

CREATE TABLE student (
    id integer DEFAULT nextval('student_seq'),
    id_person integer not null,
    university_career varchar(100) not null,
    year_degree integer not null,

    CONSTRAINT pk_student PRIMARY KEY (id, id_person),
    CONSTRAINT fk_id_person FOREIGN KEY (id_person) REFERENCES person(id)
);